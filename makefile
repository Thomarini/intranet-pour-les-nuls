
all : cours.pdf

cours.pdf : cours.tex $(wildcard */*.tex)
	lualatex cours.tex
	lualatex cours.tex

clean :
	rm -f cours.aux
	rm -f cours.log
	rm -f cours.out
	rm -f cours.pdf
	rm -f cours.toc